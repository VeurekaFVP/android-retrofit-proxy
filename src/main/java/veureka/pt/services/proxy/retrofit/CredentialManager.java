package veureka.pt.services.proxy.retrofit;

public abstract class CredentialManager {
    
    static final String ERROR_MISSING_CREDENTIALS = "error_credential_manager_missing_credentials";
    static final String ERROR_MISSING_BASE_URL = "error_credential_manager_missing_base_url";
    static final String ERROR_MISSING_USERNAME = "error_credential_manager_missing_username";
    static final String ERROR_MISSING_PASSWORD = "error_credential_manager_missing_password";
    
    protected String username;
    protected String password;
    protected String baseURL;
    private String authToken;
    
    protected CredentialManager(String username, String password, String baseURL) {
        this.baseURL = baseURL;
        this.username = username;
        this.password = password;
    }
    
    public String getUsername() {
        return username;
    }
    
    public String getPassword() {
        return password;
    }
    
    public String getBaseURL() {
        return baseURL;
    }
    
    public String getAuthToken() {
        return authToken;
    }
    
    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }
    
    public abstract void setUsername(String username);
    
    public abstract void setPassword(String password);
    
    public abstract void setBaseURL(String baseURL);
}
