package veureka.pt.services.proxy.retrofit;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.JsonObject;

import org.apache.commons.lang3.reflect.MethodUtils;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import com.google.common.io.BaseEncoding;


import io.reactivex.Single;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import veureka.pt.services.AException;
import veureka.pt.services.AndroidException;
import veureka.pt.services.aexception.BuildConfig;

import static veureka.pt.services.proxy.retrofit.CredentialManager.ERROR_MISSING_BASE_URL;
import static veureka.pt.services.proxy.retrofit.CredentialManager.ERROR_MISSING_CREDENTIALS;
import static veureka.pt.services.proxy.retrofit.CredentialManager.ERROR_MISSING_PASSWORD;
import static veureka.pt.services.proxy.retrofit.CredentialManager.ERROR_MISSING_USERNAME;

public class RetrofitProxy {

    private static final String MapsEngineProxyEndPoint = "MapsEngineProxyEndPoint";
    private static final String ERROR_METHOD_NOT_FOUND = "error_retrofit_proxy_method_not_found";
    private static final String ERROR_ILLEGAL_ACCESS = "error_retrofit_proxy_illegal_access";
    private static final String ERROR_INVOCATION_TARGET = "error_retrofit_proxy_invocation_target";
    private static final String ERROR_MISSING_AUTH_TOKEN = "error_retrofit_proxy_missing_auth_token";
    
    @SuppressWarnings("unchecked")
    public static <S> S createEndPoint(final Context context, IServicesFactory serviceRequestType, Boolean hasAuth)
            throws AException {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        if(MapsEngineProxyEndPoint.equals(serviceRequestType.toString())) {
            Log.i("Zurdo", "Agregando headers especiales para maps proxy, appId: " + context.getPackageName());
            httpClient
                    .addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Interceptor.Chain chain) throws IOException {
                            final Context myContext = context;

                            String signatureDigest = getSignature(myContext.getPackageManager(), myContext.getPackageName());
                            Request request = chain.request().newBuilder()
                                    .addHeader("X-Android-Cert", signatureDigest)
                                    .addHeader("X-Android-Package", context.getPackageName())
                                    .build();
                            return chain.proceed(request);
                        }
                    });

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(logging);

        } else {

        }

        Retrofit.Builder builder;
        Retrofit retrofit;
        CredentialManager credentials = serviceRequestType.getCredentials();
        if(credentials == null) {
            Exception e = new IllegalArgumentException("Missing credentials");
            throw AndroidException.make(context, ERROR_MISSING_CREDENTIALS, e);
        }
        if(credentials.getBaseURL() == null) {
            throw AndroidException.make(context, ERROR_MISSING_BASE_URL, new NullPointerException());
        }
        builder = new Retrofit.Builder()
                .baseUrl(credentials.getBaseURL())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .client(httpClient.build());
        
        if(hasAuth) {
            String authToken = credentials.getAuthToken();
            if(authToken == null) {
                if(TextUtils.isEmpty(credentials.getUsername())) {
                    Exception e = new IllegalArgumentException("Missing password");
                    throw AndroidException.make(context, ERROR_MISSING_USERNAME, e);
                }
                if(TextUtils.isEmpty(credentials.getPassword())) {
                    Exception e = new IllegalArgumentException("Missing password");
                    throw AndroidException.make(context, ERROR_MISSING_PASSWORD, e);
                }
                
                authToken = okhttp3.Credentials.basic(credentials.getUsername(), credentials.getPassword());
                credentials.setAuthToken(authToken);
            }
            if(!TextUtils.isEmpty(authToken)) {
                AuthenticationInterceptor interceptor = new AuthenticationInterceptor(authToken);
                httpClient.addInterceptor(interceptor);
                builder.client(httpClient.build());
            } else {
                Exception e = new IllegalArgumentException("Not AuthToken");
                throw AndroidException.make(context, ERROR_MISSING_AUTH_TOKEN, e);
            }
        }
        retrofit = builder.build();
        return retrofit.create((Class<S>) serviceRequestType.getServiceClass());
    }
    
    @SuppressWarnings("unchecked")
    public static <T, S> Single<T> handleResponse(Context context, IServiceRequestType services, S service,
                                                  JsonObject query, Object... params) throws AException {
        Object[] arguments;
        if(query != null) {
            arguments = new Object[params.length + 1];
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json"), query.toString());
            arguments[params.length] = body;
        } else {
            arguments = new Object[params.length];
        }
        for(int i = 0; i < params.length; i++) {
            arguments[i] = params[i] == null ? null : params[i];
        }
        Object result;
        try {
            result = MethodUtils.invokeMethod(service, services.getService(), arguments);
        } catch(NoSuchMethodException e) {
            throw AndroidException.make(context, ERROR_METHOD_NOT_FOUND, e);
        } catch(IllegalAccessException e) {
            throw AndroidException.make(context, ERROR_ILLEGAL_ACCESS, e);
        } catch(InvocationTargetException e) {
            throw AndroidException.make(context, ERROR_INVOCATION_TARGET, e);
        }
        return (Single<T>) result;
    }

    public static String getSignature(@NonNull PackageManager pm, @NonNull String packageName) {
        try {
            PackageInfo packageInfo = pm.getPackageInfo(packageName, PackageManager.GET_SIGNATURES);
            if (packageInfo == null
                    || packageInfo.signatures == null
                    || packageInfo.signatures.length == 0
                    || packageInfo.signatures[0] == null) {
                return null;
            }
            return signatureDigest(packageInfo.signatures[0]);
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }

    private static String signatureDigest(Signature sig) {
        byte[] signature = sig.toByteArray();
        try {
            MessageDigest md = MessageDigest.getInstance("SHA1");
            byte[] digest = md.digest(signature);
            return BaseEncoding.base16().lowerCase().encode(digest);
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }
}
