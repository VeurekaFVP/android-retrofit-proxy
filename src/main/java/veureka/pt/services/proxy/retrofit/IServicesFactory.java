package veureka.pt.services.proxy.retrofit;

import android.content.Context;

import veureka.pt.services.AException;

public interface IServicesFactory<S> {
    
    S getServices();
    
    Class<S> getServiceClass();
    
    S startEndPoint(Context context, String username, String password, String baseURL) throws AException;
    
    S startEndPoint(Context context, String token, String baseURL) throws AException;
    
    CredentialManager getCredentials();
}
